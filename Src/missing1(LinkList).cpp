#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>

#include "LinkList.h"

using namespace std;


Node* Crack_Code_2_5_Find_Circle_Start(Node* m_pHead){
    Node* n1 = m_pHead;
    Node* n2 = m_pHead;
    // Find meeting point
    while (n2->next != nullptr){
        n1 = n1->next;
        n2 = n2->next->next;
        if (n1 == n2){
        break;
        }
    }
    /* Move n1 to Head. Keep n2 at Meeting Point. Each are k
    steps*/
    n1 = m_pHead;
    /* from the Loop Start. If they move at the same pace, they
    must meet at Loop Start. */
    while (n1 != n2){
        n1 = n1->next;
        n2 = n2->next;
    }
    // Now n2 points to the start of the loop.
    return n2;
}
char a = 'a';
char b = 'b';
char c = 'c';
char d = 'd';
char e = 'e';

int main(){
   
   Node* m_pHead = NULL;
	Node* n1 = NULL;
	Node* n2 = NULL;
    Node* n3 = NULL;
    Node* n4 = NULL;

    m_pHead = new Node();
	n1 = new Node();
	n2 = new Node();
    n3 = new Node();
    n4 = new Node();

    m_pHead->data = a;
    m_pHead->next = n1;

    n1->data = b;
    n1->next = n2;

    n2->data = c;
    n2->next = n3;

    n3->data = d;
    n3->next = n4;

    n4->data = e;
    n4->next = n2;
    cout<<(char)(Crack_Code_2_5_Find_Circle_Start(m_pHead)->data);
}