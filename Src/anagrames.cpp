#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>


using namespace std;

void swap(int *xp, int *yp) 
{ 
    int temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
} 

void bubbleSort(char* pWords, int n) 
{ 
    int i, j; 
    for (i = 0; i < n-1; i++)
    for (j = 0; j < n-i-1; j++) 
        if (pWords[j] > pWords[j+1]) 
            swap(pWords[j], pWords[j+1]); 
}

void Sort_a(char *ppWords[],int narWordsLength[],int nNumWords){
    char** ppWordsCopy = new char*[nNumWords];
    for (int i = 0;i < nNumWords; i++){
        ppWordsCopy[i] = new char[narWordsLength[i]];
        strncpy(ppWordsCopy[i], ppWords[i],  narWordsLength[i]);
    }for (int i = 0; i < nNumWords; i++){
        bubbleSort(ppWordsCopy[i], narWordsLength[i]);
    }for (int i = 0; i < nNumWords; i++){
        for (int j = 0; j < nNumWords; j++){
            if (strcmp(ppWords[j], ppWordsCopy[j+1]) > 0){
                swap(ppWords[j],ppWords[j+1]);
                swap(ppWordsCopy[j],ppWordsCopy[j+1]);
            }
        }
        
    }
}
void print_subarr(char** ppWords,int narWordsLength[],int nNumWords) {
    for (int i = 0; i < nNumWords; i++)
    {
        for (int j = 0; j < narWordsLength[i]; j++)
        {
            cout << ppWords[i][j];
        }
        cout<<" ";
        
    }
    
}
int main(){
    
    char *ppWords[]= {"whhjts", "twhhjs", "hrjgs", "rghjs", "ajs", "hjccyys"};
    int narWordsLength[] = {6,6,5,5,3,7};
    int nNumWords = 6;
    //char pWords[] = {"silent"};
    //int n = sizeof(arr)/sizeof(arr[0]);
    Sort_a(ppWords,narWordsLength,nNumWords);
    //print_subarr(ppWords, narWordsLength, nNumWords);

}

