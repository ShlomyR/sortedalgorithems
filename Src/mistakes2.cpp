#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>


using namespace std;

void print_arr(char str[], int size) {
    for (int i = 0; i < size  ; i++)
    {
        if(str[i] == '\0') {
            break;
        }
        cout << str[i];
    }
    cout<<endl;
    
}

void IntToString(int number,char input_string[]){
    
    int i = 0;
    while (number != 0){
        input_string[i++] = '0' + number %10;
        number /= 10;
    }
    input_string[i] = '\0';
    
} 

int main(){
    char input_string[130];
    
    IntToString(12345,input_string);
   // print_arr(input_string, sizeof(input_string)/sizeof(input_string[0]));
   //cout<<input_string;
   string str = input_string;
   reverse(str.begin(), str.end());
   cout<<str;
}