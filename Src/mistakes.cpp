#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>


using namespace std;


float *func1(float n)
{
    float *a = &n;
    (*a) = n + 5;
    do{
        (*a)--;
    }while (*a > 0);
    return a;
}

int main(){

    cout<<*func1(50.1)<<endl;
    return 0;
}