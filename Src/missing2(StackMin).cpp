#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>


using namespace std;

struct NodeWithMin{
    private:

    public:
        int nData;
        int nMinimun;
        NodeWithMin(int Data, int Minimum){
            nData = Data;
            nMinimun =  Minimum;
        }
        ~NodeWithMin() {};
};

class CStackWithMinimum {
    private:

        stack<NodeWithMin>my_stack;

    public:
        void push1(int value){
        
            int newMin = value;

            if(my_stack.empty() == true ){
                newMin = value;

                
            }
            else if (newMin <= my_stack.top().nMinimun){
                newMin = value;
            }
            
            else{
                newMin = my_stack.top().nMinimun;
            }
            NodeWithMin node(value,newMin);
            my_stack.push(node);   
        }
        int get_min(){
            return my_stack.top().nMinimun;
        }

};

int main(){
    CStackWithMinimum x;
    x.push1(13);
    x.push1(22);
    x.push1(19);
    x.push1(5);
    x.push1(18);

    cout<<x.get_min();
}