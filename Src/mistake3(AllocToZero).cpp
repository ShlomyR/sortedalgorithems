#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>

using namespace std;


int* AllocateAndZeroMemory(int nSize){
    int* pRet = new int[nSize*sizeof(int)];
    if (pRet){
        while (nSize != 0){
            pRet[nSize--] = 0;
            //cout<<&pRet<<endl;
        }
    }
    return pRet;
}
int main(){
    AllocateAndZeroMemory(5);
}