#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>


using namespace std;


int search(int arr[], int l ,int r, int val){
    //int mid = (l + r)/2;
    while (l <= r){
        
        int mid = (l + r)/2;

        if (val == arr[mid] ){
            return mid;
        }else if (arr[l] <= arr[mid]){
            //cout<< arr[l]<<","<< arr[mid] <<endl;
            if (val > mid){
                l = l +1;
            }else if (val <= arr[mid]){
                r = mid -1;
            }else{
                r = arr[mid]-1;
            }
        }else if (val < arr[mid]){
            r = mid;
        }else if (val <= arr[r]){
            l = mid +1;
        }else{
            r = mid -1;
        }
    }
    return -1;
    
}

int main(){
    int arr[] = {1,2,4,5,7,8,9};
    int n = sizeof(arr)/sizeof(arr[0]);
    cout<<search(arr,0,n-1,7)<<endl;
}