#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>
#include <cstring>
#include<bits/stdc++.h>


using namespace std;

void swap(int *xp, int *yp) 
{ 
    int temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
}

int partione(int arr[], int l,int r){
    int piv = l-1;
    int elament_piv = arr[r];
    int size = r-l+1;
    //int i =0;
    for (int i = l; i < r; i++){
        if(arr[i] < elament_piv){
            swap(&arr[piv+1],&arr[i]);
            piv++;
            
        }
    }
    swap(&arr[piv+1],&arr[r]);
    return piv +1;
}
void printArray(int arr[], int size) 
{ 
    int i; 
    for (i = 0; i < size; i++) 
        cout << arr[i] << " "; 
    cout << endl; 
}
void quickSort(int arr[], int low, int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
        at right place */
        int pi = partione(arr, low, high);
 
        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}
 int main() 
{ 
    int arr[] = {90,98,100,1,123,132,111,101,107,3,0,1000,5}; 
    int n = sizeof(arr)/sizeof(arr[0]); 
     cout<<"Sorted array: \n"; 
    
    //partione(arr,0,n-1);
    quickSort(arr,0,n-1);
    
    printArray(arr, n);
     
    return 0; 
}