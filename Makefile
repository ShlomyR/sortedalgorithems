GCC=/usr/bin/g++

sortedalgorithems: Src/anagrames.o Src/BinarySerch.o Src/BobleSort.o Src/bocketsort.o Src/MergeSort.o Src/mistakes.o Src/Quicksort.o Src/SplitSortedArray.o Src/mistakes2.o
	$(GCC) Src/anagrames.o -o anagrames
	$(GCC) Src/BinarySerch.o -o BinarySerch
	$(GCC) Src/BobleSort.o -o BobleSort
	$(GCC) Src/bocketsort.o -o bocketsort
	$(GCC) Src/MergeSort.o -o MergeSort
	$(GCC) Src/mistakes.o -o mistakes
	$(GCC) Src/Quicksort.o -o Quicksort
	$(GCC) Src/mistakes2.o -o mistakes2
	$(GCC) Src/SplitSortedArray.o -o SplitSortedArray

clean:
	rm  Src/anagrames.o Src/BinarySerch.o Src/BobleSort.o Src/bocketsort.o Src/MergeSort.o Src/mistakes.o Src/Quicksort.o Src/SplitSortedArray.o  Src/mistakes.o Src/mistakes2.o anagrames BinarySerch BobleSort bocketsort MergeSort mistakes Quicksort mistakes2 SplitSortedArray